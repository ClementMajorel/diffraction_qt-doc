

<!doctype html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Rectangular Aperture &#8212; Diffraction_qt 1.0.0 documentation</title>
    <link rel="stylesheet" href="_static/bizstyle.css" type="text/css" />
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
    
    <script id="documentation_options" data-url_root="./" src="_static/documentation_options.js"></script>
    <script src="_static/jquery.js"></script>
    <script src="_static/underscore.js"></script>
    <script src="_static/doctools.js"></script>
    <script src="_static/language_data.js"></script>
    <script async="async" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/latest.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <script src="_static/bizstyle.js"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Double slit Aperture" href="double_slit.html" />
    <link rel="prev" title="Diffraction : Huygens-Fresnel principle and Fraunhofer Approximation" href="principle_diffraction.html" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <!--[if lt IE 9]>
    <script src="_static/css3-mediaqueries.js"></script>
    <![endif]-->
  </head><body>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul></pre>
<ul>
    <li><a href="index.html">Home</a> |</li>
    <li>
        <li class="nav-item nav-item-this"><a href="">Rectangular Aperture</a></li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <div class="section" id="rectangular-aperture">
<span id="rectangular-section-label"></span><h1>Rectangular Aperture<a class="headerlink" href="#rectangular-aperture" title="Permalink to this headline">¶</a></h1>
<div class="section" id="theoretical-formalism">
<h2>Theoretical formalism<a class="headerlink" href="#theoretical-formalism" title="Permalink to this headline">¶</a></h2>
<p>As a reminder of the section <a class="reference internal" href="principle_diffraction.html#diffraction-section-label"><span class="std std-ref">Diffraction : Huygens-Fresnel principle and Fraunhofer Approximation</span></a>, the general expression of the amplitude at the position P in the Fourier plane is</p>
<div class="math notranslate nohighlight">
\[\Psi(P) =
     \Psi(u,v) =
     \int_{\Sigma}t(x,y)\underline{\Psi_{0}^{-}}e^{-i2\pi(u x+v y)}\mathrm{d}\Sigma\]</div>
<p>with <cite>t(x,y)</cite> the transmittance function, <cite>(u,v)</cite> the spatial frequency wave numbers, <span class="math notranslate nohighlight">\(\underline{\Psi_{0}^{-}}\)</span> the plane wave amplitude just before
the holed screen and <span class="math notranslate nohighlight">\(\mathrm{d}\Sigma\)</span> is the area of this screen.</p>
<p>For a rectangular aperture the transmittance function <cite>t(x,y)</cite> is defined as the product of rectangular functions</p>
<div class="math notranslate nohighlight">
\[t(x,y) =
\text{rect}(\frac{x}{a})\text{rect}(\frac{y}{b})\]</div>
<p>where <em>a</em> is the dimension of the aperture along the Ox direction and <em>b</em> along Oy.</p>
<p>This definition determines the upper and lower integration limits for the both integrals along the <em>x</em> and <em>y</em> directions.</p>
<p>After having calculated the two Fourier transforms, we find the amplitude</p>
<div class="math notranslate nohighlight">
\[\Psi(u,v) =
\underline{\Psi_{0}^{-}}ab\, \text{sinc}(ua)\text{sinc}(vb)\]</div>
<p>with <span class="math notranslate nohighlight">\(u=\frac{x_\text{i}}{\lambda f}\)</span> and <span class="math notranslate nohighlight">\(v=\frac{y_\text{i}}{\lambda f}\)</span>.</p>
<p>Therefore, the intensity is equal to</p>
<div class="math notranslate nohighlight">
\[I(u,v) =
\vert \Psi(u,v) \vert^2 =
I_0\, \text{sinc}^2(ua)\text{sinc}^2(vb)\]</div>
<p>where <span class="math notranslate nohighlight">\(I_0=\vert\underline{\Psi_{0}^{-}}\vert^2 a^2 b^2\)</span>.</p>
<p>If <cite>a=b</cite>, the interfringe on the diffraction pattern is equal to <span class="math notranslate nohighlight">\(i=\frac{\lambda f}{a}=\frac{\lambda f}{b}\)</span> in both Ox and Oy directions.
Now if we lengthen the opening in one direction, for example if we have <span class="math notranslate nohighlight">\(b=100a\)</span>, the interfringe is reduced by a factor 100 in the same direction.
In addition, if the laser wavelength or the focal distance increases, the size of the diffraction pattern increases too.</p>
<p>Thus, the interest of the &lt;<a class="reference external" href="https://gitlab.com/diffraction_qt/diffraction_qt/-/blob/master/diffraction/diffraction_simple_square_hole.py">https://gitlab.com/diffraction_qt/diffraction_qt/-/blob/master/diffraction/diffraction_simple_square_hole.py</a>&gt;`_ file is to study these different
effects.</p>
<div class="section" id="graphic-interface-description">
<h3>Graphic interface description<a class="headerlink" href="#graphic-interface-description" title="Permalink to this headline">¶</a></h3>
<p>For the rectangular aperture shape, the graphic interface is divided into four parts (see <a class="reference internal" href="#graphic-label"><span class="std std-numref">Fig. 3</span></a>).</p>
<div class="figure align-default" id="graphic-label">
<span id="interf-label"></span><img alt="_images/inter_graph_square_hole.png" src="_images/inter_graph_square_hole.png" />
<p class="caption"><span class="caption-number">Fig. 3 </span><span class="caption-text">Image of the graphic interface</span><a class="headerlink" href="#graphic-label" title="Permalink to this image">¶</a></p>
</div>
<p>Zone (a) contains all variables that can be modified by the user, (b) is a 2D map of the aperture qualitatively showing the aspect ratio between <em>a</em> and <em>b</em>,
(c) represents the 2D diffraction pattern map corresponding to the aperture and the graph on (d) represents the normalized intensity along the Oy=0 axis.
Curve (d) gives additional informations over (c) such as the values of the normalized intensity (dashed black lines) of the two highest peaks and the full width
at half maximum of the main peak (dashed red line).
The axes of the map on the figure (c) are in meters like the horizontal axis on figure (d).
Before to give some examples, we will describe the variables area.</p>
</div>
<div class="section" id="variables-area">
<h3>Variables area<a class="headerlink" href="#variables-area" title="Permalink to this headline">¶</a></h3>
<div class="figure align-default" id="id1">
<span id="variable-label"></span><img alt="_images/variables_area.png" src="_images/variables_area.png" />
<p class="caption"><span class="caption-number">Fig. 4 </span><span class="caption-text">Zoom on the “Variables” window</span><a class="headerlink" href="#id1" title="Permalink to this image">¶</a></p>
</div>
<p>Explanation of each parameter:</p>
<ul class="simple">
<li><p><strong>a</strong> : dimension of the aperture along Ox (in micrometers)</p></li>
<li><p><strong>b</strong> : dimension of the aperture along Oy (in micrometers)</p></li>
<li><p><strong>lamda</strong> : laser wavelength (in nanometers)</p></li>
<li><p><strong>focal</strong> : lens focal length (in centimeters)</p></li>
<li><p><strong>High Contrast box</strong> : increase the contrast of the 2D diffraction pattern map by reducing the upper limit of the color bar from 1 to 0.02</p></li>
<li><p><strong>Plot button</strong> : starts the calculation</p></li>
</ul>
</div>
</div>
<div class="section" id="effect-of-a-b-lamda-and-focal">
<h2>Effect of a, b, lamda and focal<a class="headerlink" href="#effect-of-a-b-lamda-and-focal" title="Permalink to this headline">¶</a></h2>
<p>The following two images show the evolution of the aperture and the diffraction pattern map if we have <span class="math notranslate nohighlight">\(a=b=55 \mu \text{m}\)</span> and <span class="math notranslate nohighlight">\(a=55 \mu \text{m}\)</span> and <span class="math notranslate nohighlight">\(b=110 \mu \text{m}\)</span>.
In the second case, the opening is lengthened by a factor 2 along the direction Oy and the interfringe is contracted by this same factor.</p>
<div class="figure align-default" id="id2">
<span id="effect-a-b-label"></span><img alt="_images/aperture_function_a_b.png" src="_images/aperture_function_a_b.png" />
<p class="caption"><span class="caption-number">Fig. 5 </span><span class="caption-text">Examples of apertures for two pairs of (a,b)</span><a class="headerlink" href="#id2" title="Permalink to this image">¶</a></p>
</div>
<div class="figure align-default" id="id3">
<span id="effect-a-b-2d-map-label"></span><img alt="_images/diffraction_pattern_function_a_b.png" src="_images/diffraction_pattern_function_a_b.png" />
<p class="caption"><span class="caption-number">Fig. 6 </span><span class="caption-text">Examples of 2D diffraction maps for two pairs of (a,b)</span><a class="headerlink" href="#id3" title="Permalink to this image">¶</a></p>
</div>
<p>As said previously, an increase in the laser wavelength or the lens focal length induce an increase in the interfringe, so it is like a zoom on the 2D diffraction map
(see <a class="reference internal" href="#diffraction-lamda-label"><span class="std std-numref">Fig. 7</span></a>).
We have made calculations with <span class="math notranslate nohighlight">\(\lambda = 400 \text{nm}\)</span> (blue in visible spectrum) and <span class="math notranslate nohighlight">\(\lambda = 800 \text{nm}\)</span> (red in visible spectrum).</p>
<div class="figure align-default" id="diffraction-lamda-label">
<span id="effect-lamda-label"></span><img alt="_images/effet_lamda_diffraction_map.png" src="_images/effet_lamda_diffraction_map.png" />
<p class="caption"><span class="caption-number">Fig. 7 </span><span class="caption-text">Effect of the laser wavelength on the diffraction pattern</span><a class="headerlink" href="#diffraction-lamda-label" title="Permalink to this image">¶</a></p>
</div>
</div>
<div class="section" id="high-contrast-box">
<h2>High contrast box<a class="headerlink" href="#high-contrast-box" title="Permalink to this headline">¶</a></h2>
<p>The <strong>High contrast</strong> check box allows to increase the contrast on the diffraction 2d map by modifying the upper color bar limit (see <a class="reference internal" href="#high-contrast-label"><span class="std std-numref">Fig. 8</span></a>).</p>
<div class="figure align-default" id="high-contrast-label">
<span id="effect-hight-contrast-label"></span><img alt="_images/high_contrast_effect.png" src="_images/high_contrast_effect.png" />
<p class="caption"><span class="caption-number">Fig. 8 </span><span class="caption-text">Effect of the <strong>High contrast</strong> check box on the diffraction pattern</span><a class="headerlink" href="#high-contrast-label" title="Permalink to this image">¶</a></p>
</div>
</div>
</div>


            <div class="clearer"></div>
          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
<h3><a href="index.html">Table of Contents</a></h3>
<p class="caption"><span class="caption-text">Contents:</span></p>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="principle_diffraction.html">Diffraction : Huygens-Fresnel principle and Fraunhofer Approximation</a></li>
<li class="toctree-l1 current"><a class="current reference internal" href="#">Rectangular Aperture</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#theoretical-formalism">Theoretical formalism</a></li>
<li class="toctree-l2"><a class="reference internal" href="#effect-of-a-b-lamda-and-focal">Effect of a, b, lamda and focal</a></li>
<li class="toctree-l2"><a class="reference internal" href="#high-contrast-box">High contrast box</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="double_slit.html">Double slit Aperture</a></li>
</ul>

  <h3><a href="index.html">Table of Contents</a></h3>
  <ul>
<li><a class="reference internal" href="#">Rectangular Aperture</a><ul>
<li><a class="reference internal" href="#theoretical-formalism">Theoretical formalism</a><ul>
<li><a class="reference internal" href="#graphic-interface-description">Graphic interface description</a></li>
<li><a class="reference internal" href="#variables-area">Variables area</a></li>
</ul>
</li>
<li><a class="reference internal" href="#effect-of-a-b-lamda-and-focal">Effect of a, b, lamda and focal</a></li>
<li><a class="reference internal" href="#high-contrast-box">High contrast box</a></li>
</ul>
</li>
</ul>

  <h4>Previous topic</h4>
  <p class="topless"><a href="principle_diffraction.html"
                        title="previous chapter">Diffraction : Huygens-Fresnel principle and Fraunhofer Approximation</a></p>
  <h4>Next topic</h4>
  <p class="topless"><a href="double_slit.html"
                        title="next chapter">Double slit Aperture</a></p>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" />
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>

    <div class="footer" role="contentinfo">
        &#169; Copyright 2020, Clément Majorel.
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 3.2.1.
    </div>
  </body>
</html>