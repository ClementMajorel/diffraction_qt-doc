

<!doctype html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Diffraction : Huygens-Fresnel principle and Fraunhofer Approximation &#8212; Diffraction_qt 1.0.0 documentation</title>
    <link rel="stylesheet" href="_static/bizstyle.css" type="text/css" />
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
    
    <script id="documentation_options" data-url_root="./" src="_static/documentation_options.js"></script>
    <script src="_static/jquery.js"></script>
    <script src="_static/underscore.js"></script>
    <script src="_static/doctools.js"></script>
    <script src="_static/language_data.js"></script>
    <script async="async" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/latest.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <script src="_static/bizstyle.js"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Rectangular Aperture" href="square_hole.html" />
    <link rel="prev" title="Welcome to diffraction_qt’s documentation!" href="index.html" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <!--[if lt IE 9]>
    <script src="_static/css3-mediaqueries.js"></script>
    <![endif]-->
  </head><body>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul></pre>
<ul>
    <li><a href="index.html">Home</a> |</li>
    <li>
        <li class="nav-item nav-item-this"><a href="">Diffraction : Huygens-Fresnel principle and Fraunhofer Approximation</a></li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <div class="section" id="diffraction-huygens-fresnel-principle-and-fraunhofer-approximation">
<span id="diffraction-section-label"></span><h1>Diffraction : Huygens-Fresnel principle and Fraunhofer Approximation<a class="headerlink" href="#diffraction-huygens-fresnel-principle-and-fraunhofer-approximation" title="Permalink to this headline">¶</a></h1>
<p>Let us consider the optical system on the following figure <a class="reference internal" href="#setup-label"><span class="std std-numref">Fig. 1</span></a>.</p>
<div class="figure align-default" id="setup-label">
<span id="exp-setup-label"></span><img alt="_images/diffraction_montage_4f.png" src="_images/diffraction_montage_4f.png" />
<p class="caption"><span class="caption-number">Fig. 1 </span><span class="caption-text">Schematic representation of the experimental setup</span><a class="headerlink" href="#setup-label" title="Permalink to this image">¶</a></p>
</div>
<p>A monochromatic point source <span class="math notranslate nohighlight">\(S_0\)</span> of wavelength <span class="math notranslate nohighlight">\(\lambda\)</span> is located at the focal length <span class="math notranslate nohighlight">\(f_1\)</span> of a first lens (<span class="math notranslate nohighlight">\(L_1\)</span>), this lens generates a plane
wave <span class="math notranslate nohighlight">\(\underline{\Psi_{0}^{-}}\)</span> (parallel to Oz) from the incident spherical wave.
This plane wave goes towards the pierced screen (D) and the diffraction phenomenon occurs at long distance because only a small part of the plane wave passes through the screen.
The second lens (L2) makes it possible to observe this interference phenomenon at infinity by reducing the Fourier space to a finite distance corresponding to the focal length <span class="math notranslate nohighlight">\(f_2\)</span> of
(L2) (where we put an observation screen (E)).</p>
<p>Throughout this part, we will develop the mathematical formalism used to describe the phenomenon of diffraction (Huygens-Fresnel principle, transmittance definition, Fraunhofer approximation, etc).</p>
<div class="section" id="huygens-fresnel-principle">
<h2>Huygens-Fresnel principle<a class="headerlink" href="#huygens-fresnel-principle" title="Permalink to this headline">¶</a></h2>
<p>The Huygens-Fresnel principle is based on the wave behavior of light. For the diffraction phenomenon, this principle says that each point of an aperture illuminated by a plane
wave becomes a point source.
All these points emit spherical waves which will interfere with each other.
Mathematically, that means the wave function <span class="math notranslate nohighlight">\(\Psi(P)\)</span> at the position <span class="math notranslate nohighlight">\(P=(x_{\text{i}},y_{\text{i}},z_{\text{i}})\)</span> on the observation screen (E) is defined by</p>
<div class="math notranslate nohighlight">
\[\Psi(P)=
    \int_{\Sigma}\underline{\Psi_{0}^{+}}(M)\frac{e^{ikr}}{r}\mathrm{d}\Sigma ,\]</div>
<p>where <em>M=(x,y, 0)</em> is an arbitrary position on the perforated screen, <span class="math notranslate nohighlight">\(r=\vert\vert\mathbf{MP}\vert\vert=\sqrt{(x-x_{i})^{2}+(y-y_{i})^{2}+z_{i}^{2}}\)</span> the norm of the vector <span class="math notranslate nohighlight">\(\mathbf{MP}\)</span>
(see <a class="reference internal" href="#fraunhofer-label"><span class="std std-numref">Fig. 2</span></a>), <span class="math notranslate nohighlight">\(k=\frac{2\pi}{\lambda}\)</span> the wave number of the light, <span class="math notranslate nohighlight">\(\underline{\Psi_{0}^{+}}(M)\)</span> is the wave function just
after the perforated screen (see <a class="reference internal" href="#setup-label"><span class="std std-numref">Fig. 1</span></a>) and <span class="math notranslate nohighlight">\(\mathrm{d}\Sigma\)</span> is the surface of this screen.</p>
<div class="figure align-default" id="fraunhofer-label">
<span id="fraunh-label"></span><img alt="_images/diffraction_fraunhofer_schema.png" src="_images/diffraction_fraunhofer_schema.png" />
<p class="caption"><span class="caption-number">Fig. 2 </span><span class="caption-text">Schematic representation of the propagation angles of light between the perforated screen (D) and the observation screen (E)</span><a class="headerlink" href="#fraunhofer-label" title="Permalink to this image">¶</a></p>
</div>
</div>
<div class="section" id="fraunhofer-approximation">
<h2>Fraunhofer approximation<a class="headerlink" href="#fraunhofer-approximation" title="Permalink to this headline">¶</a></h2>
<p>The Fraunhofer diffraction consists in viewing the diffraction pattern at a long distance from the diffracting object.
Thus we can make the Taylor series of <em>r</em></p>
<div class="math notranslate nohighlight">
\[r\simeq r_{i}-\frac{xx_{i}+yy_{i}}{r_{i}}\simeq r_{i}-(\alpha x+\beta y)\]</div>
<p>with <span class="math notranslate nohighlight">\(r_{i}=\sqrt{x_{i}^{2}+y_{i}^{2}+z_{i}^{2}}\)</span>, <span class="math notranslate nohighlight">\(\alpha=\dfrac{x_{i}}{r_{i}}\)</span> and <span class="math notranslate nohighlight">\(\beta = \dfrac{y_{i}}{r_{i}}\)</span>.</p>
<p>By replacing the approximate expression of <em>r</em> in <span class="math notranslate nohighlight">\(\Psi(P)\)</span>, we obtain</p>
<div class="math notranslate nohighlight">
\[\Psi(P)\simeq\int_{\Sigma}\underline{\Psi_{0}^{+}}(x,y)\frac{e^{ikr_{i}}}{r_{i}}e^{-i\mathbf{k}\cdot\mathbf{OM}}\mathrm{d}\Sigma\]</div>
<p>with <span class="math notranslate nohighlight">\(\mathbf{k}=\frac{2\pi}{\lambda}\frac{\mathbf{OP}}{OP}\)</span>.</p>
<p>This expression is valid only for a perfectly flat screen (D).
If its surface <span class="math notranslate nohighlight">\(\mathrm{d}\Sigma\)</span> is not flat we must introduce a factor <em>Q</em>, depending of the angle between the vector <span class="math notranslate nohighlight">\(\mathbf{MP}\)</span>
and the normal to <span class="math notranslate nohighlight">\(\mathrm{d}\Sigma\)</span> in <em>M</em>, such as</p>
<div class="math notranslate nohighlight">
\[\Psi(P)\simeq Q\int_{\Sigma}\underline{\Psi_{0}^{+}}(x,y)\frac{e^{ikr_{i}}}{r_{i}}e^{-i\mathbf{k}\cdot\mathbf{OM}}\mathrm{d}\Sigma.\]</div>
<p>The last expression is the fondamental relation of the diffraction in the Fraunhofer approximation.</p>
<p>The integrals are performed on the variables <cite>(x, y)</cite>, thus the term <span class="math notranslate nohighlight">\(Q\dfrac{e^{ikr_{i}}}{r_{i}}\)</span> can be considered as a constant.
In other words, all the diffraction information is contained under the integral and we can reduce the previous expression to</p>
<div class="math notranslate nohighlight">
\[\Psi(P)\simeq \int_{\Sigma}\underline{\Psi_{0}^{+}}(x,y)e^{-i\mathbf{k}\cdot\mathbf{OM}}\mathrm{d}\Sigma .\]</div>
</div>
<div class="section" id="spatial-frequency-wavenumber">
<h2>Spatial frequency wavenumber<a class="headerlink" href="#spatial-frequency-wavenumber" title="Permalink to this headline">¶</a></h2>
<p>We introduce the well-know nomenclature of spatial frequency wave numbers <cite>(u,v)</cite> as</p>
<div class="math notranslate nohighlight">
\[u = \frac{\alpha}{\lambda}\quad\text{and}\quad v = \frac{\beta}{\lambda}.\]</div>
<p>We define previously</p>
<div class="math notranslate nohighlight">
\[\alpha=\frac{x_{i}}{r_{i}}\quad\text{and}\quad\beta = \frac{y_{i}}{r_{i}},\]</div>
<p>thus using the sine definition we easily observe that <span class="math notranslate nohighlight">\(\alpha=\sin\theta_{x}\)</span> and <span class="math notranslate nohighlight">\(\beta=\sin\theta_{y}\)</span> with <span class="math notranslate nohighlight">\(\theta_{x}\)</span> and <span class="math notranslate nohighlight">\(\theta_{y}\)</span>
the angles of propagation represented on the figure <a class="reference internal" href="#fraunhofer-label"><span class="std std-numref">Fig. 2</span></a>.</p>
<p>Consequently, the relation between the spatial frequency wave numbers and the angles <span class="math notranslate nohighlight">\(\theta_{x}\)</span> and <span class="math notranslate nohighlight">\(\theta_{y}\)</span> are</p>
<div class="math notranslate nohighlight">
\[\alpha=\frac{\sin\theta_{x}}{\lambda}\quad\text{and}\quad\beta = \frac{\sin\theta_{y}}{\lambda}.\]</div>
<p>These two relations mean that for a point P on the observation screen, a single pair of angles, and only one, are associated with this point.
So, only light beams with corresponding angles will interfere in P.</p>
<p>If we introduce <em>u</em> and <em>v</em> in <span class="math notranslate nohighlight">\(\Psi(P)\)</span> we obtain</p>
<div class="math notranslate nohighlight">
\[\Psi(P) =
     \Psi(u,v) =
     \int_{\Sigma}\underline{\Psi_{0}^{+}}(x,y)e^{-i2\pi(u x+v y)}\mathrm{d}\Sigma.\]</div>
</div>
<div class="section" id="transmittance-function">
<h2>Transmittance function<a class="headerlink" href="#transmittance-function" title="Permalink to this headline">¶</a></h2>
<p>For a plane wave perpendicular to the diaphragm (D) (propagating along Oz), we have got a simple relation between the wave function <span class="math notranslate nohighlight">\(\underline{\Psi_{0}^{+}}\)</span> directly after the screen
and the incident wave function <span class="math notranslate nohighlight">\(\underline{\Psi_{0}^{-}}\)</span>.
They are related by the transmittance <cite>t(x,y)</cite> as</p>
<div class="math notranslate nohighlight">
\[\underline{\Psi_{0}^{+}} =
            t(x,y)\underline{\Psi_{0}^{-}}.\]</div>
<p>This function is equal to 1 if <em>M</em> is part of the opening of (D) and equal to 0 otherwise.</p>
<p>In that conditions (normal incident plane wave), we have got</p>
<div class="math notranslate nohighlight">
\[\Psi(u,v)\simeq\underline{\Psi_{0}^{-}}\int_{\Sigma}t(x,y)e^{-i2\pi(u x+v y)}\mathrm{d}\Sigma.\]</div>
<p>Since <span class="math notranslate nohighlight">\(I(u,v)=\vert\Psi(u,v)\vert^2\)</span> :</p>
<div class="math notranslate nohighlight">
\[I(u,v)\simeq\vert\int_{\Sigma}t(x,y)e^{-i2\pi(u x+v y)}\mathrm{d}\Sigma\vert^2,\]</div>
<p>thus, if we omit the coefficient <span class="math notranslate nohighlight">\(\dfrac{Q^2}{r_i^2}\)</span>, the intensity is proportional to the Fourier transform of <em>t(x,y)</em>.</p>
<p>Two examples of transmittance function are given in the section <a class="reference internal" href="square_hole.html#rectangular-section-label"><span class="std std-ref">Rectangular Aperture</span></a> and <a class="reference internal" href="double_slit.html#double-slit-section-label"><span class="std std-ref">Double slit Aperture</span></a>.</p>
</div>
</div>


            <div class="clearer"></div>
          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
<h3><a href="index.html">Table of Contents</a></h3>
<p class="caption"><span class="caption-text">Contents:</span></p>
<ul class="current">
<li class="toctree-l1 current"><a class="current reference internal" href="#">Diffraction : Huygens-Fresnel principle and Fraunhofer Approximation</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#huygens-fresnel-principle">Huygens-Fresnel principle</a></li>
<li class="toctree-l2"><a class="reference internal" href="#fraunhofer-approximation">Fraunhofer approximation</a></li>
<li class="toctree-l2"><a class="reference internal" href="#spatial-frequency-wavenumber">Spatial frequency wavenumber</a></li>
<li class="toctree-l2"><a class="reference internal" href="#transmittance-function">Transmittance function</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="square_hole.html">Rectangular Aperture</a></li>
<li class="toctree-l1"><a class="reference internal" href="double_slit.html">Double slit Aperture</a></li>
</ul>

  <h3><a href="index.html">Table of Contents</a></h3>
  <ul>
<li><a class="reference internal" href="#">Diffraction : Huygens-Fresnel principle and Fraunhofer Approximation</a><ul>
<li><a class="reference internal" href="#huygens-fresnel-principle">Huygens-Fresnel principle</a></li>
<li><a class="reference internal" href="#fraunhofer-approximation">Fraunhofer approximation</a></li>
<li><a class="reference internal" href="#spatial-frequency-wavenumber">Spatial frequency wavenumber</a></li>
<li><a class="reference internal" href="#transmittance-function">Transmittance function</a></li>
</ul>
</li>
</ul>

  <h4>Previous topic</h4>
  <p class="topless"><a href="index.html"
                        title="previous chapter">Welcome to diffraction_qt’s documentation!</a></p>
  <h4>Next topic</h4>
  <p class="topless"><a href="square_hole.html"
                        title="next chapter">Rectangular Aperture</a></p>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" />
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>

    <div class="footer" role="contentinfo">
        &#169; Copyright 2020, Clément Majorel.
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 3.2.1.
    </div>
  </body>
</html>